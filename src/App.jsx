import { useState } from "react";
import { MdOutlineKeyboardDoubleArrowRight, MdOutlineKeyboardDoubleArrowLeft, MdKeyboardArrowRight, MdKeyboardArrowLeft } from "react-icons/md";

export default function App() {

  const [leftContainer, setLeftContainer] = useState([
    { 'code': 'JS', 'checkbox': false },
    { 'code': 'HTML', 'checkbox': false },
    { 'code': 'CSS', 'checkbox': false },
    { 'code': 'TS', 'checkbox': false }
  ]);

  const [rightContainer, setRightContainer] = useState([
    { 'code': 'React', 'checkbox': false },
    { 'code': 'Angular', 'checkbox': false },
    { 'code': 'Vue', 'checkbox': false },
    { 'code': 'Svelte', 'checkbox': false }
  ]);

  let countOfLeftChecked = leftContainer.reduce((count, currentItem) => {
    return currentItem.checkbox ? count + 1 : count;
  }, 0)

  let countOfRightChecked = rightContainer.reduce((count, currentItem) => {
    return currentItem.checkbox ? count + 1 : count;
  }, 0)

  function shiftItemsRight() {

    let uncheckedItems = leftContainer.filter((item) => !item['checkbox'])

    let checkedItems = leftContainer.filter((item) => {
      if (item['checkbox']) {
        countOfLeftChecked -= 1;
        item['checkbox'] = false;
        return item
      }
    })

    setLeftContainer(uncheckedItems)
    setRightContainer(rightContainer.concat(checkedItems))

  }

  function shiftAllItemsRight() {

    const newRightContainer = leftContainer.map((item) => {
      if (item['checkbox']) {
        item['checkbox'] = false;
      }
      return item;

    })

    setRightContainer(rightContainer.concat(newRightContainer))
    setLeftContainer([])
  }

  function shiftItemsleft() {

    let uncheckedItems = rightContainer.filter((item) => !item['checkbox'])

    let checkedItems = rightContainer.filter((item) => {
      if (item['checkbox']) {
        countOfRightChecked -= 1;
        item['checkbox'] = false;
        return item;
      }
    })

    setRightContainer(uncheckedItems)
    setLeftContainer(leftContainer.concat(checkedItems))
  }

  function shiftAllItemsleft() {

    const newLeftContainer = rightContainer.map((item) => {

      if (item['checkbox']) {
        item['checkbox'] = false;
      }
      return item;

    })

    setLeftContainer(leftContainer.concat(newLeftContainer))
    setRightContainer([])
  }

  function handleLeftContainer(index) {

    setLeftContainer(leftContainer.map((item, itemIndex) => {

      if (index === itemIndex) {

        return {
          ...item, checkbox: !item.checkbox
        }
      } else {
        return item
      }
    }))

  }

  function handleRightContainer(index) {

    setRightContainer(rightContainer.map((item, itemIndex) => {

      if (index === itemIndex) {
        return {
          ...item, checkbox: !item.checkbox
        }
      } else {
        return item
      }


    }))
  }


  return (
    <>
      <nav className="text-center text-[30px] my-8 mx-auto border-b-2 border-gray-200 pb-4">Transfer List</nav>

      <section>
        <div className="max-w-[1200px] mx-auto my-8">
          <div className="flex mx-auto">
            <div id="left" className="container1 flex-col w-[45%] border-2 border-gray-500 px-5 pt-6">

              {leftContainer.map((item, index) => (
                <div key={index}>

                  <input className=" mr-3"
                    type="checkbox"
                    checked={item['checkbox']}
                    onChange={() => {
                      handleLeftContainer(index)
                    }} />

                  <span>{item['code']}</span>
                </div>

              ))}

            </div>

            <div className="flex-col px-10 py-5 justify-center border-2 border-gray-500 ">
              <div>
                <button onClick={() => shiftAllItemsRight()}
                  disabled={leftContainer.length === 0 ? true : false}
                  className=" bg-gray-200 border-2 rounded m-2 p-1 opacity-100  disabled:bg-white disabled:opacity-30 ">
                  <MdOutlineKeyboardDoubleArrowRight />
                </button>
              </div>

              <div className="text-center">
                <button onClick={() => shiftItemsRight()}
                  disabled={countOfLeftChecked > 0 ? false : true}
                  className=" bg-gray-200 border-2 rounded m-2 p-1 opacity-100  disabled:bg-white disabled:opacity-30 "  >
                  <MdKeyboardArrowRight />
                </button>
              </div>

              <div className="text-center">
                <button onClick={() => shiftItemsleft()}
                  disabled={countOfRightChecked > 0 ? false : true}
                  className=" bg-gray-200 border-2 rounded m-2 p-1 opacity-100  disabled:bg-white disabled:opacity-30 ">
                  <MdKeyboardArrowLeft />
                </button>
              </div>

              <div>
                <button onClick={() => shiftAllItemsleft()}
                  disabled={rightContainer.length === 0 ? true : false}
                  className=" bg-gray-200 border-2 rounded m-2 p-1 opacity-100  disabled:bg-white disabled:opacity-30 ">
                  <MdOutlineKeyboardDoubleArrowLeft />
                </button>
              </div>
            </div>

            <div id="right" className="container2 flex-col w-[45%] border-2 border-gray-500 px-5 pt-6">
              {rightContainer.map((item, index) => (
                <div key={index}>

                  <input className=" mr-3"
                    type="checkbox"
                    checked={item['checkbox']}
                    onChange={(e) => {
                      handleRightContainer(index)
                    }} />

                  <span>{item['code']}</span>
                </div>

              ))}

            </div>

          </div>

        </div>
      </section>
    </>
  )
}